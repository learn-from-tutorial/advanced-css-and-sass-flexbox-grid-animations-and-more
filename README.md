# advanced-css-and-sass

> Advanced CSS and Sass Take Your CSS to the Next Level

## Build Setup

``` bash
# install dependencies
$ yarn install

# serve with hot reload at localhost:3000
$ yarn run dev

# build for production and launch server
$ yarn run build
$ yarn start

# generate static project
$ yarn run generate
```

For detailed explanation on how things work, checkout [Nuxt.js docs](https://nuxtjs.org).

## CSS tools
# clip-path: polygon(0 0, 100% 0, 100% 75vh, 0 100%)
# https://bennettfeely.com/clippy/

# อันไหนสำคัญให้ดูจากผลรวม 
$ (inline, IDs, Classes, Elements) -> (0, 0, 3, 2) = 5 
